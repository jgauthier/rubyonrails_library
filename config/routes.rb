Rails.application.routes.draw do
  root 'static_pages#home'
  resources :books #do
  #   member do
  #     get 'buy'
  #     resources :comments
  #   end
  #   collection do
  #     get 'bestof'
  #   end
  # end
  #
  # # Pour les API
  # namespace :api do
  #   namespace :v1 do
  #     get 'version' => 'api#version'
  #   end
  # end
  #
  # # Mettre des contraintes
  # constraints(subdomain:/^admin$/) do
  #   scope module: 'admin' do
  #     resources :books
  #   end
  # end
  #
  # root 'static_pages#home'
  #
  # get 'users/:username' => 'users#show', as: :show_user

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
