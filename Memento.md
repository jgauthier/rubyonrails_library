# Notes sur le framwork RoR

## L'installation 

Toujours un peu laborieuse. 

Ce qui fonctionne : pour ubuntu 18.04 


Mise à jour des outils de développement

```
sudo apt-get update
sudo apt-get install build-essential libffi-dev libssl-dev libreadline-dev zlib1g-dev libsqlite3-dev ruby-dev zlib1g-dev liblzma-dev
```

Installe Node.js et git (si git pas installé)

```
sudo apt-get install nodejs git
```

Installe rbenv

```
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
source ~/.bashrc
```

Installe Ruby

```
rbenv install <version>
rbenv global <version>
```

Installe Rails

```
gem install rails -v <version>
```

Installe Yarn

```
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - 
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

## Lancer le serveur Rails 

```
rails s
```

Vérifier dans un navigateur que le serveur tourne à l'adresse ip généré par défault (généralement localhost sur le port 3000) :

`http://127.0.0.1:3000/`

## Créer un nouveau projet

Dans une console : 

```
rails new <nomDuProjet>
```

A partir de la on retrouve des méchanismes semblable à cake, de génération d'arborescence du projet en MVC. 
Ainsi que des conventions de nommage automatique. 
La même règle de nommage d'un objet au singulier > donne des mvc aux pluriels. 

## Le fichier Gemfile

Le fichier Gemfile génère le fichier Gemfile.lock (comme pour composer chez cake).

## Les routes 

Dans le fichier config/routes 

la simple déclaration de ressource génère automatiquement les routes : `resources :books`

Compatible json (url + .json affiche le json)

## Les controllers 

application_controller.rb = le controller par default
books_controller.rb = le controller de la ressource book
concerns/ = module en commun entre les différents controller

## La console rails

Se lance dans un terminal avec la commande : `rails c`

Permet d'acceder aux données. Quelques commandes : 

Book.all
Book.count
Book.order(title: :asc)
Book.order(title: :desc)
book = Book.create({title: "Mon titre", author: "Mon auteur"})
Book.destroy

## Les vues 

Utilisent bootstrap 
application.html.er = le layout de base 

Deux formats : .erb .json 

<% = syntaxe de balise pour du ruby

<%= yield %> ca appel ce qu'il y a dans les vues spécifiques

## Autre

coffeescript = version simplifié de javascript

config/environnement/ gère les différents environnement de dev, prod et test

Locales : simple_form

## Quelques commandes 

En console normal, utilisateur normal, dans le projet concerné 

rake routes // liste les routes 
rake db:migrate  // créer la table books dans a base
rails c // lance le terminal rails 