class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  # before_filter :set_locale
  #
  # def default_url_options(options={})
  #   logger.debug "default_url-options is passed options: #{options.inspect}\n"
  #   { local: I18n.locale }
  # end
  #
  #  # recupere la langue de la requete http
  # private
  #   def http_header_locale
  #     request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  #   end
  #
  # def set_locale
  #   I18n.locale = params[:locale] || http_header_locale || I18n.default_locale
  # end
end
